## 工程介绍

Oauth2.0是为了对接第三方，让第三方有资格来访问我们指定的资源，如果是本地服务也不提供给别的厂家等，那就没必要加入Oauth2.0验证，不过token生成、验证机制还是可以借鉴的

#### 1. 创建WebSecurityConfig

#### 2. 创建AuthorizationServerConfig

#### 3. 启动服务，浏览器里输入以下地址
- 授权模式
```url
http://localhost:8080/oauth/authorize?client_id=godliu&client_secret=helloworld&response_type=code
```
如果未登录过，那么就进去了登录页面(spring自提供)，登录成功(WebsecurityConfig配置的用户信息)后即可返回我们配置的授权码,然后用这个授权码再次去获取token
```$xslt
https://www.baidu.com/?code=SlMOu6
```
基于授权码获取token
```$xslt
http://localhost:8080/oauth/token?client_id=godliu&client_secret=helloworld&grant_type=authorization_code&code=SlMOu6
```
- 密码模式
```$xslt
http://localhost:8080/oauth/token?client_id=godliu&client_secret=helloworld&grant_type=password&username=wang&password=123
```
- 简化模式
```$xslt
http://localhost:8080/oauth/authorize?client_id=godliu&client_secret=helloworld&response_type=token
```
- 客户端模式
```$xslt
http://localhost:8080/oauth/token?client_id=godliu&client_secret=helloworld&grant_type=client_credentials
```

#### 4. 完成了认证服务后，开始资源服务配置定义


