package com.liu.mq.producter.producter.controller;

import com.google.gson.Gson;
import com.liu.mq.producter.producter.cfg.JRedisConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.Map;

@Api(tags = "redis服务")
@RestController
public class RedisController {

    @ApiOperation(value = "添加一条信息到redis中")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "key",value = "key键",required = true,dataTypeClass = String.class),
        @ApiImplicitParam(name = "msg",value = "value值",required = true,dataTypeClass = String.class)
    })
    @GetMapping(value = "/somevalue",produces = {"application/json;charset=UTF-8"})
    public String setMsg(String key,String msg){
        Map<String,String> map = new HashMap<>();
        Jedis jedis = null;
        JedisPool jedisPool = JRedisConfig.getInstance().getJedisPool();
        try {
            jedis = jedisPool.getResource();
            jedis.set(key,msg);
            map.put(key,jedis.get(key));
        }catch (Exception e){
            e.printStackTrace();
        }

        return new Gson().toJson(map);
    }

    @ApiOperation(value = "根据key查询消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key",value = "key键",required = true,dataTypeClass = String.class),
    })
    @GetMapping(value = "/getsomevalue",produces = {"application/json;charset=UTF-8"})
    public String getMsg(String key){
        Map<String,String> map = new HashMap<>();
        Jedis jedis = null;
        JedisPool jedisPool = JRedisConfig.getInstance().getJedisPool();
        try {
            jedis = jedisPool.getResource();
            map.put(key,jedis.get(key));
        }catch (Exception e){
            e.printStackTrace();
        }

        return new Gson().toJson(map);
    }

}
