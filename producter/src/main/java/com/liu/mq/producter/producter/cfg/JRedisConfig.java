package com.liu.mq.producter.producter.cfg;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * JRedis 配置
 */
@Component
public class JRedisConfig {


    private static String HOST;
    private static String PASSWD;
    private static int PORT;
    private static JRedisConfig jRedisConfig;
    private static JedisPool jedisPool;

    private JRedisConfig(){

    }

    @Value("${spring.redis.host}")
    public void setHOST(String host) {
       HOST = host;
    }

    @Value("${spring.redis.password}")
    public void setPASSWD(String passwd) {
        PASSWD = passwd;
    }

    @Value("${spring.redis.port}")
    public void setPORT(int port) {
        PORT = port;
    }

    public static JRedisConfig getInstance(){
        if(jRedisConfig == null){
            synchronized (JRedisConfig.class){
                if(jRedisConfig == null)
                    jRedisConfig = new JRedisConfig();
            }
        }
        return jRedisConfig;
    }
    public JedisPool getJedisPool(){
        System.out.println("host:"+HOST);
        if(jedisPool == null || jedisPool.isClosed()){
            synchronized (JedisPool.class){
                if(jedisPool == null)
                    jedisPool = new JedisPool(new JedisPoolConfig(), HOST,PORT,30,PASSWD);
            }
        }
        return  jedisPool;
    }
}
