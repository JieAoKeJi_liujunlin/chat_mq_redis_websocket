package com.liu.mq.producter.producter.controller;

import com.google.gson.Gson;
import com.liu.mq.producter.producter.entity.User;
import com.liu.mq.producter.producter.mapper.UserMapper;
import com.liu.mq.producter.producter.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@EnableAutoConfiguration
@RequestMapping("/user")
@Api(tags = "用户服务")
public class UserContoller {

    @Resource
    UserMapper userMapper;

    @Resource
    UserService userService;

    @ApiOperation(value = "通过mapper添加用户，并返回")
    @PostMapping(value = "/add",produces = {"application/json;charset=UTF-8"})
    public String addUser(@RequestBody User user){
        Map<String,Object> result = new HashMap<>();
        result.put("code","1");
        result.put("msg","success");
        int id =  userMapper.insertSelective(user);
        User user1 = userMapper.selectByPrimaryKey(id);
        result.put("user",user1);
        return new Gson().toJson(result);
    }

    @ApiOperation(value = "通过service添加用户，并返回")
    @PostMapping(value = "/adduser",produces = {"application/json;charset=UTF-8"})
    public String addUserByService(@RequestBody User user){
        Map<String,Object> result = new HashMap<>();
        result.put("code","1");
        result.put("msg","success");
        User user1 = userService.addUser(user);
        result.put("user",user1);
        return new Gson().toJson(result);
    }

    @ApiOperation(value = "获取用户")
    @ApiImplicitParam(value = "用户id",name = "userId",required = true,dataTypeClass = String.class)
    @GetMapping(value = "/get",produces = {"application/json;charset=UTF-8"})
    public String getUserInfo(Integer userId){
        Map<String,Object> result = new HashMap<>();
        result.put("code","1");
        result.put("msg","success");
        User user1 = userService.getUserById(userId);
        result.put("user",user1);
        return new Gson().toJson(result);
    }

    @ApiOperation(value = "分页获取用户")
    @ApiImplicitParams({
        @ApiImplicitParam(value = "页码",name = "pageNum",required = true,dataTypeClass = Integer.class),
        @ApiImplicitParam(value = "每页条数",name = "pageSize",required = true,dataTypeClass = Integer.class)
    })
    @GetMapping(value = "/getUsersOfPage",produces = {"application/json;charset=UTF-8"})
    public String getUsersOfPage(Integer pageNum,Integer pageSize){
        Map<String,Object> result = new HashMap<>();
        result.put("code","1");
        result.put("msg","success");
        List<User> user1 = userService.getUsersByPage(pageNum,pageSize);
        result.put("user",user1);
        return new Gson().toJson(result);
    }
}
