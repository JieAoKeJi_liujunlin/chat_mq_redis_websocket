package com.liu.mq.producter.producter.service.impl;

import com.github.pagehelper.PageHelper;
import com.liu.mq.producter.producter.entity.User;
import com.liu.mq.producter.producter.mapper.UserMapper;
import com.liu.mq.producter.producter.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {

    // 注入mapper类
    @Resource
    private UserMapper userMapper;

    @Override
    public User addUser(User user) {
        if(user.getId() != null)
            user.setId(null);
        user.setDepre(true);
        int id =  userMapper.insertSelective(user);
        return userMapper.selectByPrimaryKey(user.getId());
    }

    @Override
    public User getUserById(int id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<User> getUsersByPage(int pageNum, int pageSize) {
        List<User> result = null;
        try {
            // 调用pagehelper分页，采用starPage方式。starPage应放在Mapper查询函数之前
            PageHelper.startPage(pageNum, pageSize); //每页的大小为pageSize，查询第page页的结果
            PageHelper.orderBy("id ASC "); //进行分页结果的排序
            result = userMapper.selectUser();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
