package com.liu.mq.producter.producter.service;

import com.liu.mq.producter.producter.entity.User;

import java.util.List;

public interface UserService {

    User addUser(User user);

    User getUserById(int id);

    List<User> getUsersByPage(int pageNum,int pageSize);
}
