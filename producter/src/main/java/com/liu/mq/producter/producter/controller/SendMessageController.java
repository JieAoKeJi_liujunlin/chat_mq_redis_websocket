package com.liu.mq.producter.producter.controller;

import com.google.gson.Gson;
import com.liu.mq.producter.producter.utils.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Api(tags = "rabbitmq服务")
@RestController
public class SendMessageController {
    @Autowired
    RabbitTemplate rabbitTemplate;  //使用RabbitTemplate,这提供了接收/发送等等方法

    @ApiOperation(value = "直连型发送消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "message",value = "要发送的消息",required = true,dataTypeClass = String.class),
    })
    @GetMapping(value = "/sendDirectMessage",produces = {"application/json;charset=UTF-8"})
    public String sendDirectMessage(String message) {
        String messageId = String.valueOf(UUID.randomUUID());
        String createTime = DateUtil.getSystem();
        Map<String,Object> map=new HashMap<>();
        map.put("messageId",messageId);
        map.put("messageData",message);
        map.put("createTime",createTime);
        //将消息携带绑定键值：TestDirectRouting 发送到交换机TestDirectExchange
        rabbitTemplate.convertAndSend("TestDirectExchange", "TestDirectRouting", map);
        return new Gson().toJson(map);
    }

    @ApiOperation(value = "扇型发送消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "message",value = "要发送的消息",required = true,dataTypeClass = String.class),
    })
    @GetMapping(value = "/sendFanoutMessage",produces = {"application/json;charset=UTF-8"})
    public String sendFanoutMessage(String message) {
        String messageId = String.valueOf(UUID.randomUUID());
        String createTime = DateUtil.getSystem();
        Map<String, Object> map = new HashMap<>();
        map.put("messageId", messageId);
        map.put("messageData", message);
        map.put("createTime", createTime);
        rabbitTemplate.convertAndSend("fanoutExchange", null, map);
        return new Gson().toJson(map);
    }

    @ApiOperation(value = "主题型发送消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "message",value = "要发送的消息",required = true,dataTypeClass = String.class),
    })
    @GetMapping(value = "/sendTopicMessage1",produces = {"application/json;charset=UTF-8"})
    public String sendTopicMessage1(String message) {
        String messageId = String.valueOf(UUID.randomUUID());
        String createTime = DateUtil.getSystem();
        Map<String, Object> manMap = new HashMap<>();
        manMap.put("messageId", messageId);
        manMap.put("messageData", message);
        manMap.put("createTime", createTime);
        rabbitTemplate.convertAndSend("topicExchange", "topic.man", manMap);
        return new Gson().toJson(manMap);
    }

    @ApiOperation(value = "主题型发送消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "message",value = "要发送的消息",required = true,dataTypeClass = String.class),
    })
    @GetMapping(value = "/sendTopicMessage2",produces = {"application/json;charset=UTF-8"})
    public String sendTopicMessage2(String message) {
        String messageId = String.valueOf(UUID.randomUUID());
        String createTime = DateUtil.getSystem();
        Map<String, Object> womanMap = new HashMap<>();
        womanMap.put("messageId", messageId);
        womanMap.put("messageData", message);
        womanMap.put("createTime", createTime);
        rabbitTemplate.convertAndSend("topicExchange", "topic.woman", womanMap);
        return new Gson().toJson(womanMap);
    }

    @ApiOperation(value = "主题型发送消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "message",value = "要发送的消息",required = true,dataTypeClass = String.class),
    })
    @GetMapping(value = "/sendTopicMessage3",produces = {"application/json;charset=UTF-8"})
    public String sendTopicMessage3(String message) {
        String messageId = String.valueOf(UUID.randomUUID());
        String createTime = DateUtil.getSystem();
        Map<String, Object> womanMap = new HashMap<>();
        womanMap.put("messageId", messageId);
        womanMap.put("messageData", message);
        womanMap.put("createTime", createTime);
        rabbitTemplate.convertAndSend("topicExchange", "something.topic", womanMap);
        return new Gson().toJson(womanMap);
    }

    @GetMapping(value = "/sendTopicMessage4",produces = {"application/json;charset=UTF-8"})
    public String sendTopicMessage4(String message) {
        String messageId = String.valueOf(UUID.randomUUID());
        String createTime = DateUtil.getSystem();
        Map<String, Object> womanMap = new HashMap<>();
        womanMap.put("messageId", messageId);
        womanMap.put("messageData", message);
        womanMap.put("createTime", createTime);
        rabbitTemplate.convertAndSend("topicExchange", "topic.man.subtopic", womanMap);
        return new Gson().toJson(womanMap);
    }
}
