package com.liu.mq.producter.producter.controller;

import com.google.gson.Gson;
import com.liu.mq.producter.producter.entity.User;
import org.springframework.web.bind.annotation.*;

@RestController
public class Test {

    @GetMapping("/hello")
    public String hello(String name){
        return "hello:" + name;
    }

    @GetMapping("/world")
    public String helloworld(){
        return "hello";
    }

    @PostMapping("/jake")
    public String hellojake(String name){
        return "welcome " + name;
    }

    @PostMapping("/user")
    public String addUser(@RequestBody User user){
        System.out.println(user.toString());
        return new Gson().toJson(user);
    }
}
