package com.liu.mq.producter.producter.utils;

import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具
 */
public class DateUtil {

    public static String getSystem(){
        return getSystemDate(null);
    }

    public static String getSystemDate(String formatTime){
        String format = "yyyy-MM-dd HH:mm:ss";
        if(!StringUtils.isEmpty(formatTime))
            format = formatTime;
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static void main(String[] args) {
        SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-dd");
        Calendar c = Calendar.getInstance();
        int day_of_week = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (day_of_week == 0)
            day_of_week = 7;
        c.add(Calendar.DATE, -day_of_week + 1);
        System.out.println(format.format(c.getTime()));
    }
}
