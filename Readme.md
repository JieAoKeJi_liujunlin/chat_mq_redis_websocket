## 项目介绍
该项目准备融合mq、redis、websocket完成在线聊天后台程序，结合前端工程实现聊天工程
1. 项目前提准备
    - 安装rabbitmq
    - 安装redis  

2. 创建聚合工程chat_mq_redis_websocket，创建2个module工程
    - producter 消息创建
    - customer  消息消费
3. 配置2个module工程application.yml文件，连接mq和redis
    - 配置mq的连接属性
    - 配置redis的连接属性
4. mq根据特点将实现直连型交换机、扇形交换机、主题交换机
    - direct 直连型交换机(消息队列和交换机一对一)
    - fanout 扇型交换机(无视路由规则，消息都分发到绑定的队列上)
    - topic 主题交换机(消息被交换机通过路由规则分发到对应的队列上)
    完成3种队列、交换机、消费消息的创建
    ```
    /cfg mq 配置 redis 配置
    /controller  放置发送消息的服务
    /receiver  放置消费消息的类 
    ```
5. pom加入redis依赖，这里为了方便开发采用jedis方式,安装docker-redis
，由于docker版本的redis没有redis.conf配置文件，所以自己需要下载一个配置文件
并挂载到redis的镜像容器里,/myredis/conf为host主机配置文件地址，redis-server是以现在的目录进行启动redis
要下载源码然后解压，从里面找到redis.conf拷到对应的目录下
    ```
    docker run -v /myredis/conf:/usr/local/etc/redis --name myredis redis redis-server /usr/local/etc/redis/redis.conf
    ```
6. 开始引入mysql驱动、mybiats包，进行数据库配置并完成mybatis实现xml和注解形式的sql配置
    1. 在application.yml文件里配置mysql相关信息
    2. mybatis-springboot官网查询
    ```
    http://mybatis.org/spring-boot-starter/mybatis-spring-boot-autoconfigure/
   ```
7. 准备mybatis和jpa混合使用，单表、简单的查询直接jpa实现，复杂的多表查询用mybatis
   1. 引入mybatis的generator代码生成器，完成几张表的代码生成
        - pom里引入mybatis的maven代码生成器插件，通过maven来生成代码，并指定配置文件地址
        - 在project的resources目录下，创建mybatis-generator文件夹，然后创建generator.properties和generatorConfig.xml对自动生成代码进行配置
        - 在ide的右边maven找到插件，运行mybatis-generator:generate即可把数据库表生成代码
        - 特别注意下：将生产的entity、mapper、dao等文件夹一定和主程序ProducterApplication放同一目录下，要不各种找不到配置
   2. 引入阿里数据库连接池druid，并进行配置，但是springboot2不支持直接属性配置，所以在工程的cfg文件夹里加个配置文件DruidConfig.java
   3. 配置mybatis属性，加入实体类别名映射和mapper xml文件地址，在ProducterApplication里加入mapscan扫描，指定mapper接口路径
   4. 这里例子UserController里提供了2种方式，一种用传统的service封装，一种直接采用自动生成的mapper查询
   5. 引入分页功能com.github.pagehelper
        - service里写分页方法
        - mapper.xml文件里写入对应的分页方式
8. 用docker安装Elasticsearch，使用下非关系数据库保存聊天记录，支持海量数据查询
    - 下载比较老的版本6.8.4 
    ```
     docker pull elasticsearch:6.8.4
    ```
    - 参考docker官网操作
    ```
    $ docker network create esnetwork
    $ docker run -d --name elasticsearch --net esnetwork -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:6.8.4
    ```
    - 服务运行后，进入docker里修改部分配置文件
    ```
    docker exec -it es /bin/bash
    cd config
    vi elasticsearch.yml
    ```
    添加如下内容，注意冒号后的空格，保存后，访问http://ip:9200,如果有返回，则运行正常
    ```
    http.cors.enable: true
    http.cors.allow-origin: "*"
    ```
9. 安装了nginx，复习下反向代理、负载均衡
10. mybatis的insert返回是影响的行数，是0或1，只有到mapperxml里进行配置才会绑定主键id，可查询对象，
如UserMapper.xml里insertSelective方法的配置useGeneratedKeys="true" keyProperty="id"
11. druid使用，提供了数据库连接池，为了节省JDBC反复连接数据库、断开等导致的资源巨大浪费
，druid的优点是还能监控数据库状况，并有web页面显示，在yml文件里配置了druid的监控配置
输入以下地址可以看到监控
```angular2
http://localhost:8888/mq/druid/index.html
```  
12.完成数据库主从配置后，开始用druid来做多数据源连接，主库写，从库读

13.通过引入spring cloud gateway，实现了基本的get/post方法路由转发
  - 配置路由，实现了多个条件通过and的方式连接
  - 配置了过滤器，通过rewritePath实现了将http://localhost:8231/api/hello转换为
  http://localhost:8888/mq/hello
  
14.创建自定义的过滤器，通过yml的配置和代码配合实现自定义的各类过滤器，有全局的、有局部的，留意代码
  - 全局过滤器，可以直接注册为bean
  - 局部过滤器，需要依赖GatewayFilterFactory才行
  继承GatewayFilter和GlobalFilter的类，有ordered接口实现，返回的值越小权重越大，越会优先进行过滤
  
15.引入slf4j日志系统，在yml里进行配置，在resource文件夹下创建logback-spring.xml的日志配置文件，
将日志保存到项目根目录的logs下

16.创建新工程oauth_authorize_server，搭建oauth2验证
- oauth的目的是为了让第三方应用能或者认证然后访问我们提供的认证资源
  
  







