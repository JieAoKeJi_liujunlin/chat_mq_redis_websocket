package com.liu.ourchat.customer.receiver;

import io.swagger.annotations.Api;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RabbitListener(queues = "fanout.B")
@Api(tags = "扇形接收器")
public class FanoutBReceiver {
    @RabbitHandler
    public void process(Map testMessage) {
        System.out.println("FanoutBReceiver消费者收到消息  : " +testMessage.toString());
    }

}
