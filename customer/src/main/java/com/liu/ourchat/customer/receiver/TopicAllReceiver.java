package com.liu.ourchat.customer.receiver;

import io.swagger.annotations.Api;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListeners;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RabbitListeners({
//        @RabbitListener(queues = "topic.man"),
        @RabbitListener(queues = "topic.woman"),
        @RabbitListener(queues = "topic.both")
})
@Api(tags = "主题型接收器")
public class TopicAllReceiver {
    @RabbitHandler
    public void process(Map testMessage) {
        System.out.println("TopicAllReceiver消费者收到消息  : " + testMessage.toString());
    }
}
