package com.god.liu.gateway.filters.authfilter;

import com.github.jknack.handlebars.internal.antlr.misc.MultiMap;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

public class CustomGatewayFilterFactory extends AbstractGatewayFilterFactory<CustomGatewayFilterFactory.Config> {

    public CustomGatewayFilterFactory(){
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(CustomGatewayFilterFactory.Config config) {

        return ((exchange, chain) -> {
            // 从yml属性里读取过滤器配置属性
            System.out.println("CustomGatewayFilterFactory:"+config.getName()+"@flag@"+config.getFlag());
            //把这2个属性，通过addheader和addrequest加入到请求里

            ServerHttpRequest request = exchange.getRequest().mutate()
                    .header("tk", "test",config.getName(),config.getFlag())
                    .build();

            return chain.filter(exchange.mutate().request(request).build());
        });
    }

    public static class Config{
        String name;
        String flag;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }
    }
}
