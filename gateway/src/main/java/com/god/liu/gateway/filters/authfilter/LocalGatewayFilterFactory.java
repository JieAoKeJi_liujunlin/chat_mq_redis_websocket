package com.god.liu.gateway.filters.authfilter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;

public class LocalGatewayFilterFactory extends AbstractGatewayFilterFactory<LocalGatewayFilterFactory.Config> {

    public LocalGatewayFilterFactory(){
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(Config config) {
        return new LocalFilter();
    }

    public static class Config{

    }
}
