package com.god.liu.gateway;

import com.god.liu.gateway.filters.authfilter.AuthFilter;
import com.god.liu.gateway.filters.authfilter.CustomGatewayFilterFactory;
import com.god.liu.gateway.filters.authfilter.LocalGatewayFilterFactory;
import com.god.liu.gateway.filters.authfilter.TimeGatewayFilterFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.filter.factory.StripPrefixGatewayFilterFactory;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.pattern.PathPatternParser;

@SpringBootApplication
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }


    /**
     * 自定义网关路由
     * @param builder
     * @return
     */
   /* @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        StripPrefixGatewayFilterFactory.Config config = new StripPrefixGatewayFilterFactory.Config();
        config.setParts(1);
        String[] methods = new String[2];
        methods[0] = "GET";
        methods[1] = "POST";
        return builder.routes()
                .route(p -> p
                        .method(methods).and()
                        .path("/api/**")//请求路径
                        //.and().query("name","^q[0-9a-zA-z]?.*")// q开头的参数做转发，其它的放弃---注意这里只能抽取get方法的参数
                        .filters(f -> f.addRequestHeader("Hello", "World")
                                .rewritePath("^/api","/mq")//重定向请求路径
                                .filter(new AuthFilter()))//这里通过代码配置了自定义的过滤器
                        .uri("http://localhost:8888/"))//路由host，将对网关访问的信息转到真实服务器上
                .build();
    }*/

    @Bean
    public TimeGatewayFilterFactory timeFilter() {
        return new TimeGatewayFilterFactory();
    }

    @Bean
    public CustomGatewayFilterFactory cussFilter() {
        return new CustomGatewayFilterFactory();
    }

    @Bean
    public AuthFilter authFilter(){
        return new AuthFilter();
    }

    @Bean
    public LocalGatewayFilterFactory loll(){
        return new LocalGatewayFilterFactory();
    }

    @Bean
    public CorsWebFilter corsFilter() {
        CorsConfiguration config = new CorsConfiguration();
        config.addAllowedMethod("*");//支持所有方法
        config.addAllowedOrigin("*");//跨域处理 允许所有的域
        config.addAllowedHeader("*");//支持所有请求头

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource(new PathPatternParser());
        source.registerCorsConfiguration("/**", config);//匹配所有请求

        return new CorsWebFilter(source);
    }
}
